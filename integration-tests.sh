#!/bin/bash
cd "$(dirname "$0")" || exit "$?"

# The purpose of this script is to start mongo running (via docker) so that we
# have a real mongo instance to test against. The actual tests are writtin
# inside test/integration folder.

if [ "$GITLAB_CI" == "" ]; then
    # If GITLAB_CI is not defined, then we know that we are running the
    # integration test locally. If we're running this locally, then we have to
    # start up the mongo container. We start it on a non-standard port so that
    # if the developer already has mongo running, then we don't needlessly fail
    # or accidentally wipe that db.
    container_name=dbmanager_mongo_container
    # "start" will recognize the container already is running. Only if its not do we attempt to create it with "run" command
    docker start "$container_name" || docker run --name "$container_name" -d -p 27017:27017 mongo || exit "$?"

    export MONGO_CONNECTION_STRING="mongodb://127.0.0.1:27000"
else
    # The .gitlab-ci.yaml file uses the "mongo" service which means that within
    # gitlab runners, we don't have to deal with starting up mongo
    export MONGO_CONNECTION_STRING="mongodb://mongo:27017"
fi

nyc --cache false --reporter=html --reporter=text --report-dir=coverage/integration mocha -require esm test/integration/*.js --LOG=debug || exit "$?"
