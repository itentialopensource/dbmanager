///// example cog.js file /////

// You must first run `npm install --save @itentialopensource/dbmanager` as well
// as `npm install --save mongodb`
const { getDbObj } = require('@itentialopensource/dbmanager');

const customDatabaseName = 'customer_database'

module.exports = {
  // For now, dbmanager API is only promise-based (no callbacks). This example
  // uses node's "async/await" feature. Therefore, the cog function must be
  // declared "async"
  async cogMethod1() {
    // Always call getDbObj _inside_ of every cog method that needs to use it.
    // If the connection to mongo gets lost, and then re-established, any
    // global variables will no longer work. By calling getDbObj inside the cog
    // method, you avoid that problem.

    // The getDbObj function will automatically connect to mongo for you --
    // using whatever configuration you supplied in dbManagerConfig. (see
    // dbmanager's README.md for more information
    // https://gitlab.com/itentialopensource/dbmanager/blob/master/README.md)

    const db = await getDbObj(customDatabaseName);

    const collection = await db.collection('my_app_collection');

    await collection.insertOne({'message': 'hello world'});
  }
};

