/* global pronghornProps */
const util = require('util');
try {
  const mongodbTestImport = require('mongodb');
} catch(error) {
  throw new Error('Error: the dbmanager module now requires you to separately install mongodb. Eg: `npm install --save mongodb`. This allows you to control what version of mongodb you are using.');
}
const mongodb = require('mongodb');

const defaultConnectionName = 'default'


// Notice: all jsdoc is moved to singleton. That way the only documentation
// generated for public is for the public API. Technically this class is not
// public API.

class ConnectionManager {
    /**
     * We want to decouple the logic for managing a connection from the logic
     * that knows default location of configuration within IAP. Therefore, this
     * class does not ever access global vars like pronghornProps or
     * service_config.
     */
    constructor(dbManagerConfig) {
        this.dbCache = {};
        this.connectionCache = {};
        if (this.validateConfig(dbManagerConfig)) {
            this.dbManagerConfig = dbManagerConfig;
        }
    }

    validateConfig(dbManagerConfig) {
        if (dbManagerConfig === undefined) {
            throw new Error('No configuration found for dbManager');
        }
        if (!isRealObject(dbManagerConfig)) {
            throw new Error(`dbManagerConfig is not an object`);
        }
        for (const connectionName in dbManagerConfig) {
            const connectionConfig = dbManagerConfig[connectionName];
            if (!isRealObject(connectionConfig)) {
                throw new Error(`dbManagerConfig for connection ${connectionName} is not an object`);
            }
            if (connectionConfig.connectionString === undefined) {
                throw new Error(`connectionString was not defined in connection configuration for connections ${connectionName}`);
            }
            if (connectionConfig.connectionOptions === undefined) {
                throw new Error(`connectionOptions were not defined in connection configuration for connections ${connectionName}`);
            }
        }
        return true;
    }

    async getConnectionConfig(connectionName) {
        return this.dbManagerConfig[connectionName];
    }

    async getClient(connectionName) {
        if (this.connectionCache[connectionName] !== undefined) {
            return this.connectionCache[connectionName];
        }
        const { connectionString, connectionOptions } = await this.getConnectionConfig(connectionName);
        // Note: This should allow values of 'useNewUrlParser' within connectionOptions to override coded default of true, etc
        // TODO: document that
        this.connectionCache[connectionName] = await mongodb.MongoClient.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true, ...connectionOptions });
        return this.connectionCache[connectionName];
    }

    async getDbObj(dbName, { connectionName = defaultConnectionName } = {}) {
        if (this.dbCache[connectionName] && this.dbCache[connectionName][dbName]) {
            return this.dbCache[connectionName][dbName];
        }
        const connection = await this.getClient(connectionName);
        const dbObj = await connection.db(dbName);
        this.dbCache[connectionName] = this.dbCache[connectionName] || {};
        this.dbCache[connectionName][dbName] = dbObj;
        return dbObj;
    }

    async closeConnection(connectionName = defaultConnectionName) {
        const connection = this.connectionCache[connectionName];
        if (connection) {
            await connection.close();
            delete this.dbCache[connectionName];
            delete this.connectionCache[connectionName];
        }
        return true;
    }

    async closeAllConnections() {
      // Returns false if at least 1 failed to close.
      let result = true;
      for (const connectionName in this.connectionCache) {
          try {
              await this.closeConnection(connectionName);
          } catch(ex) {
              result = false;
          }
      }
      return result;
    }
}

// In javascript, multiple items report themselves as "object" if you merely
// run typeof. Checking constructor is the safest way to avoid false positives.
// We run `item &&` first to avoid calling ".constructor" on null/undefined.
function isRealObject(item) {
    return item && (item.constructor === Object);
}

module.exports.ConnectionManager = ConnectionManager;
