
## 1.0.2 [06-25-2020]

* adding maintainers file

See merge request itentialopensource/dbmanager!12

---

## 1.0.1 [06-25-2020]

* incrementing version to 1.0.0

See merge request itentialopensource/dbmanager!11

---

## 0.1.20 [02-06-2020]

* 📝 💥 No longer auto installing mongodb. Revamped documentation.

See merge request itentialopensource/dbmanager!10

---

## 0.1.19 [01-10-2020]

* Patch/open source

See merge request itential/sandbox/dbmanager!8

---

## 0.1.18 [01-10-2020]

* Patch/open source

See merge request itential/sandbox/dbmanager!8

---

## 0.1.17 [01-10-2020]

* Additional Documentation

See merge request itential/sandbox/dbmanager!7

---

## 0.1.16 [01-10-2020]

* Corrected the key for service_config

See merge request itential/sandbox/dbmanager!6

---

## 0.1.15 [01-09-2020]

* ✅ renamed 2 APIs (with aliases of old names); Added integration test

See merge request itential/sandbox/dbmanager!5

---

## 0.1.14 [01-09-2020]

* ✅ renamed 2 APIs (with aliases of old names); Added integration test

See merge request itential/sandbox/dbmanager!5

---

## 0.1.13 [01-08-2020]

* Patch/itentialopensource

See merge request itential/sandbox/dbmanager!4

---

## 0.1.12 [01-08-2020]

* Patch/itentialopensource

See merge request itential/sandbox/dbmanager!4

---

## 0.1.11 [01-08-2020]

* Patch/itentialopensource

See merge request itential/sandbox/dbmanager!4

---

## 0.1.10 [01-08-2020]

* Patch/itentialopensource

See merge request itential/sandbox/dbmanager!4

---

## 0.1.9 [12-19-2019]

* Updates

See merge request itential/sandbox/dbmanager!3

---

## 0.1.8 [12-11-2019]

* Update user-guide.md as there was a typo

See merge request itential/sandbox/dbmanager!2

---

## 0.1.7 [12-09-2019]

* Bug fixes and performance improvements

See commit 56e0ae3

---

## 0.1.6 [12-09-2019]

* Bug fixes and performance improvements

See commit 8712ebd

---

## 0.1.5 [12-05-2019]

* Bug fixes and performance improvements

See commit 49f80d4

---

## 0.1.4 [12-05-2019]

* Bug fixes and performance improvements

See commit e697881

---

## 0.1.3 [12-04-2019]

* Bug fixes and performance improvements

See commit c2c1ac9

---

## 0.1.2 [12-04-2019]

* Bug fixes and performance improvements

See commit 8793b71

---

## 0.1.1 [12-04-2019]

* Bug fixes and performance improvements

See commit c2ec8a9

---
