## Functions

<dl>
<dt><a href="#validateConfig">validateConfig(connectionConfig)</a> ⇒ <code>boolean</code></dt>
<dd><p>validateConfig takes config as input so that the API user can take the
class and validate config any way they might want to without requiring
to create a class instance. Perhaps we should only return a boolean
here, and then throw an exception elsewhere, but I want to have good
error messages, and a single boolean primative doesn&#39;t get us that as
easily.</p>
</dd>
<dt><a href="#getConnectionConfig">getConnectionConfig(connectionName)</a> ⇒ <code>object</code></dt>
<dd><p>This will retrieve the configuration (connectionString and
 connectionOptions) for a connection. Note: all configuration must be
 already decrypted by Itential Automation Platform (IAP) for it to be used.
 Therefore, the output of this function will have the decrypted values
 of any config you encrypted using IAP&#39;s encrypt.js script.</p>
</dd>
<dt><a href="#getClient">getClient(connectionName)</a> ⇒ <code>object</code></dt>
<dd><p>Returns a mongo &#39;client&#39; object. Checks first if the connection
already exists. If it does, it returns it. If it does not, it attempts to
create one and cache it for reuse.</p>
</dd>
<dt><a href="#getDbObj">getDbObj(dbName, [{}], [&#x27;default&#x27;])</a> ⇒ <code>object</code></dt>
<dd><p>Returns a Mongo &#39;db&#39; object. Checks first if the connection
already exists. If it does, it uses that same connection. If it does not, it attempts to
create a connection (caching it for reuse), and then retrieves and returns the db
object for the database you specified.</p>
</dd>
<dt><a href="#closeConnection">closeConnection()</a> ⇒ <code>boolean</code></dt>
<dd><p>Attempts to close the specified database connections. If the connections
already exists, and remove the connections from the local cache.</p>
</dd>
<dt><a href="#closeAllConnections">closeAllConnections()</a> ⇒ <code>boolean</code></dt>
<dd><p>Attempts to close ALL database connections. If the connections
already exists, and remove the connections from the local cache.</p>
</dd>
</dl>

<a name="validateConfig"></a>

## validateConfig(connectionConfig) ⇒ <code>boolean</code>
validateConfig takes config as input so that the API user can take the
class and validate config any way they might want to without requiring
to create a class instance. Perhaps we should only return a boolean
here, and then throw an exception elsewhere, but I want to have good
error messages, and a single boolean primative doesn't get us that as
easily.

**Kind**: global function  
**Returns**: <code>boolean</code> - - returns true if valid  
**Throws**:

- <code>Exception</code> - if anything is invalid, an exception is thrown.


| Param | Type | Description |
| --- | --- | --- |
| connectionConfig | <code>object</code> | the config you want for creating a new connection. From within "dbManagerConfig" object in configuration (see README.md), this function expects only the config for one connection. In the examples from README, you would take the "default" object and pass it in here. The connectionConfig should have a properties called connectionString and connectionOptions. |

<a name="getConnectionConfig"></a>

## getConnectionConfig(connectionName) ⇒ <code>object</code>
This will retrieve the configuration (connectionString and
 connectionOptions) for a connection. Note: all configuration must be
 already decrypted by Itential Automation Platform (IAP) for it to be used.
 Therefore, the output of this function will have the decrypted values
 of any config you encrypted using IAP's encrypt.js script.

**Kind**: global function  
**Returns**: <code>object</code> - connectionConfig - same output which is valid as input for validateConfig  

| Param | Type | Description |
| --- | --- | --- |
| connectionName | <code>string</code> | the configured name for the connection  whose config you are wanting. |

<a name="getClient"></a>

## getClient(connectionName) ⇒ <code>object</code>
Returns a mongo 'client' object. Checks first if the connection
already exists. If it does, it returns it. If it does not, it attempts to
create one and cache it for reuse.

**Kind**: global function  
**Returns**: <code>object</code> - - The mongo client object: http://mongodb.github.io/node-mongodb-native/2.0/api/MongoClient.html  

| Param | Type | Description |
| --- | --- | --- |
| connectionName | <code>string</code> | The configured name for connection you want to use. |

<a name="getDbObj"></a>

## getDbObj(dbName, [{}], [&#x27;default&#x27;]) ⇒ <code>object</code>
Returns a Mongo 'db' object. Checks first if the connection
already exists. If it does, it uses that same connection. If it does not, it attempts to
create a connection (caching it for reuse), and then retrieves and returns the db
object for the database you specified.

**Kind**: global function  
**Returns**: <code>object</code> - - The mongo db object: http://mongodb.github.io/node-mongodb-native/2.0/api/Db.html  

| Param | Type | Description |
| --- | --- | --- |
| dbName | <code>string</code> | The name of the mongo database you want to use |
| [{}] | <code>object</code> | options |
| ['default'] | <code>object</code> | options.connectionName - The configured name for connection you want to use.     Normally you don't specify this. Normally you will just use the 'default' connection information which will be     the same connection information used by IAP core (aka: using the same installation of mongo that "mongoProps"     is configured to use). |

<a name="closeConnection"></a>

## closeConnection() ⇒ <code>boolean</code>
Attempts to close the specified database connections. If the connections
already exists, and remove the connections from the local cache.

**Kind**: global function  
**Returns**: <code>boolean</code> - - true if it either successfully closed or wasn't already created  
<a name="closeAllConnections"></a>

## closeAllConnections() ⇒ <code>boolean</code>
Attempts to close ALL database connections. If the connections
already exists, and remove the connections from the local cache.

**Kind**: global function  
**Returns**: <code>boolean</code> - - false if at least 1 failed to close. Otherwise true.  
