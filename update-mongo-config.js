
/*

The purpose of this script is to update all disparate locations of mongo
config. See below for why this is important.

Right now, this whole file is one big TODO

The way this will work is that you run this script on the cli, it will read
"mongoProps", then it will update the mongo connection information in the
following locations
* every mongo adapter config
* every app/adapter service config
  * specifically this script will only modify the 'default' connection


Why Manage Config This Way?
====

Ideally, We want a single source of truth for mongo connection information.
However, simply due to the legacy design of IAP, that's really hard (if not
impossible) to %100 accomplish. For that reaason, this module took the
different approach of trying to auto-update multiple places at once for you
instead of truly consolidating all mongo connection config into one location.
Perhaps in the future, this concept can be more natively accomplished.

For example, IAP core requires the existence of a config section called
"mongoProps".  However, for many existing customers, they will also have a
mongo adapter with it's _own_ place to configure mongo connection settings.
Already, IAP, by itself, has for a while required copy/pasting the same mongo
config in different locations.

Also, mongoProps is not formatted the same way needed for mongo driver. Under
the hood, IAP is using an npm modules called a bit of custom code in order to
transform that config information into the exact format needed by mongodb node
driver. This configuration ("mongoProps") will always point to a database named
"pronghorn", and this database should never be used by custom apps/adapters.

Even though 2019.2 has service config, there is no way to share config between
apps/services. Except via profiles, but profile configuration is not promised
to remain the same way it works now, and so it is not a long-term stable idea
to use it to store config. However, people have always updated props.json
(which turns into profiles) so this is still supported by this module.
*/


// eventually I think this should be added to the "bin" section of package.json

// this logic should whitelist version of IAP it supports migrating.  that way
// if future versions change too much, but the customer never upgraded from an
// older version of this tool, then there is a nice error message telling them
// they need to upgrade this tool



