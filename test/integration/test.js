
const mocha = require('mocha');
const td = require('testdouble');
const {
  expect
} = require('chai');
const pathToCodeUnderTest = '../../index';
const pathToSingleton = '../../src/singleton';


// In order to build stable tests, we must ensure that all tests follow certain
// guidelines. This "wrapper" function will enforce those guidelines
async function wrapTest(testFunction) {
  // Mock out Itential's global "log" variable. We do this in each test to
  // really ensure that there's no chance of a previous test changing the
  // value of "log" and affecting future tests.
  global.log = {
    error() {},
    warn() {},
    info() {},
    debug() {},
    trace() {},
    spam() {},
  };

  // We have to make sure that the dbManagerConfig is always compatible with
  // the integration test environment. This logic ensures it uses env vars in
  // the way that integration-tests.sh uses -- since that script will be used
  // to create the docker integration env for these tests to run against.  We
  // do this in each test to really ensure that there's no chance of a
  // previous test changing the value of "log" and affecting future tests.
  global.pronghornProps = {
    dbManagerConfig: {
      default: {
        connectionString: process.env["MONGO_CONNECTION_STRING"],
        connectionOptions: process.env["MONGO_OPTION_JSON"] && JSON.parse(process.env["MONGO_OPTION_JSON"]) || {},
      }
    }
  }
  try {
    // It's important that we delete cache for the singleton each time. That
    // way every single test starts with a very clean slate.  There is
    // technically some logic that only runs when the singleton does not
    // already exist. If a test wants to assume singleton is already created,
    // it can create it however desired in its own setup phase.

    // Note: testdouble (the tool we're using for mocking) needs all
    // 'require' statements to happen _after_ any td.replace calls. In order
    // to respect that, we dynamically require the pathToSingleton here
    // inside the finally clause (ie, after testFunction already ran)
    const { deleteSingleton } = require(pathToSingleton);
    deleteSingleton()
    await testFunction();
  } finally {
    // If we don't close the connection, then the process doesn't close, so the
    // test mocha never finishes. Therefore, we create this "decorator" function to
    // ensure that all of our tests will correctly close any open connections. Yes,
    // it's true that the way we close connections is by calling one of the APIs
    // we're building, but since that API is, itself, tested below, we can trust it
    // to work for all of our other tests.

    // Note: testdouble (the tool we're using for mocking) needs all
    // 'require' statements to happen _after_ any td.replace calls. In order
    // to respect that, we dynamically require the pathToSingleton here
    // inside the finally clause (ie, after testFunction already ran)
    const { closeAllConnections } = require(pathToCodeUnderTest);
    await closeAllConnections();
  }
}

describe('integration tests', function() {
  it('testing getClient, closeAllConnections, and attaching to mongo events', function(done) {
    wrapTest(async function() {
      //// Test Data
      const allConnectionNames = ['default', 'foobarConnection'];
      const connectionsClosed = {};

      //// Setup
      global.pronghornProps.dbManagerConfig.foobarConnection = global.pronghornProps.dbManagerConfig.default;
      // Establish multiple connections during setup phase. This must be done
      // before calling "closeAllConnections" if we want the test to confirm connections are really closed
      const { getDbObj, getClient } = require(pathToCodeUnderTest);
      for (const connectionName of allConnectionNames) {
        await getDbObj('customer_database', { connectionName });
        expect(getClient(connectionName)).to.not.equal(undefined)
        let connectionWasClosed = false;
        (await getClient(connectionName)).on('close', function() {
          connectionsClosed[connectionName] = true;
        });
      }

      //// Run
      const { closeAllConnections } = require(pathToCodeUnderTest);
      closeAllConnections();

      //// Assert
      setTimeout(() => {
        for (const connectionName of allConnectionNames) {
          expect(connectionsClosed[connectionName]).to.equal(true, `connection ${connectionName} was not closed`);
        }
        done();
      }, 500);
    });
  });
  it('basic', async function() {
    await wrapTest(async function() {
      //// Run
      const { getDbObj } = require(pathToCodeUnderTest);
      const db = await getDbObj('customer_database_name');
      const collection = await db.collection('foobar');
      await collection.insertOne({'message': 'hello world'});

      //// Assert
      // By not throwing an error, this test passes
    });
  });
});


