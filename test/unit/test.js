/* global pronghornProps */
/* eslint-env node, mocha */
const util = require('util');
const td = require('testdouble');
const anything = td.matchers.anything();
const {
  expect
} = require('chai');

let mockDbMgrConfig = {
  "default": {
    "connectionString": "mongodb://127.0.0.1:27017",
    "connectionOptions": {useNewUrlParser: true},
  }
};


let mongoDbMgr;
let mongodb = td.object();
let connection = td.object();


describe('singleton', () => {
  beforeEach(() => {
    global.pronghornProps = {};
    // JSON stringify/parse will create a deep copy to prevent any side-effects
    global.pronghornProps.dbManagerConfig = JSON.parse(JSON.stringify(mockDbMgrConfig));
    mongodb.MongoClient = td.object(['connect']);
    mongodb.MongoClient.connect = td.function();
    td.replace('mongodb', mongodb);
    const m = require('mongodb');
    connection.db = td.function();
    const indexPath = '../../index';
    delete require.cache[require.resolve(indexPath)];
    mongoDbMgr = require(indexPath);
  });

  afterEach(() => {
    td.reset();
  });


  describe('getDbObj', () => {

    it('should create a new DB if not found in cache and return a db handle and close connection when done', async () => {
      td.when(mongodb.MongoClient.connect(anything, anything)).thenReturn(connection);
      td.when(connection.db(anything)).thenReturn(td.object());
      try {
        const db = await mongoDbMgr.getDbObj('foo');
        expect(db).to.exist;
      } finally {
        connection.close = td.function();
        expect(await mongoDbMgr.closeConnection('default')).to.eql(true);
      }
    });
  });

  describe('getDbObjError', () => {

    it('should throw exception if MongoClient throws exception', async () => {
      let exceptionThrown = false;
      td.when(mongodb.MongoClient.connect(anything, anything)).thenThrow(new Error('fake error'));
      td.explain(mongodb.MongoClient.connect);
      try {
        await mongoDbMgr.getDbObj('foo');
      } catch (ex) {
        exceptionThrown = true;
        expect(ex.message).to.contain('fake error');
      }
      expect(exceptionThrown).to.equal(true);
    });

    it('should throw exception if connection.db throws exception', async () => {
      td.when(mongodb.MongoClient.connect(anything, anything)).thenReturn(connection);
      td.when(connection.db(anything)).thenThrow(new Error('fake error'));
      let exceptionThrown = false;
      try {
        await mongoDbMgr.getDbObj('bar');
      } catch (ex) {
        exceptionThrown = true;
        expect(ex.message).to.contain('fake error');
      }
      expect(exceptionThrown).to.equal(true);
    });
  });

  describe('getConnectionConfig', () => {
    it('should return valid connection config', async () => {
      const config = await mongoDbMgr.getConnectionConfig('default');
      expect(config).to.eql(mockDbMgrConfig.default);
    });

    it('should return undefined for unavailable db config', async () => {
      const config = await mongoDbMgr.getConnectionConfig('foo');
      expect(config).to.eql(undefined);
    });
  });

  describe('closeConnection', () => {
    it('should close connection if connection is open', async () => {
      td.replace(mongodb.MongoClient, 'connect');
      td.when(mongodb.MongoClient.connect(anything, anything)).thenReturn(td.object());
      const connObj = await mongoDbMgr.getClient('default');
      connObj.close = td.func();
      expect(await mongoDbMgr.closeConnection('default')).to.eql(true);
    });
  });

  describe('closeAllConnections', () => {
    it('should close all open connections', async () => {
      td.when(mongodb.MongoClient.connect(anything, anything)).thenReturn(td.object());
      const connObj = await mongoDbMgr.getClient('default');
      connObj.close = td.func();
      expect(await mongoDbMgr.closeAllConnections()).to.eql(true);
    });

    it('should return false if closeConnection throws Exception for at least 1 connection', async () => {
      td.when(mongodb.MongoClient.connect(anything, anything)).thenReturn(td.object());
      const connObj = await mongoDbMgr.getClient('default');
      connObj.close = td.func();
      td.when(connObj.close()).thenThrow(new Error('fake error'));
      expect(await mongoDbMgr.closeAllConnections()).to.eql(false);
    });
  });

  describe('validateConfig', () => {
    it('should throw an error if a client is incorrectly configured - missing connectionString', async () => {
      let exceptionThrown = false;
      const mockDbMgrConfig = {
        'misconfigured': {
          "dbs": [
            "db3Client1"
          ]
        }
      };
      try {
        await mongoDbMgr.validateConfig(mockDbMgrConfig);
      } catch (e) {
        expect(e).to.exist;
        exceptionThrown = true;
        expect(e.message).to.contain('connectionString was not defined in connection configuration for connections');
      }
      expect(exceptionThrown).to.equal(true);
    });
    it('should throw an error if a client is incorrectly configured - missing connectionOptions', async () => {
      let exceptionThrown = false;
      const mockDbMgrConfig = {
        'misconfigured': {
          "connectionString": "mongodb://127.0.0.1:27017",
          "dbs": [
            "db3Client1"
          ]
        }
      };
      try {
        await mongoDbMgr.validateConfig(mockDbMgrConfig);
      } catch (e) {
        expect(e).to.exist;
        exceptionThrown = true;
        expect(e.message).to.contain('connectionOptions were not defined in connection configuration for connections');
      }
      expect(exceptionThrown).to.equal(true);
    });

    it('should throw an error if dbConfig is not found', async () => {
      let exceptionThrown = false;
      const mockDbMgrConfig = undefined;
      try {
        await mongoDbMgr.validateConfig(mockDbMgrConfig);
      } catch (e) {
        expect(e).to.exist;
        exceptionThrown = true;
        expect(e.message).to.contain('No configuration found for dbManager');
      }
      expect(exceptionThrown).to.equal(true);
    });

    it('should throw an error if dbConfig is not a real object', async () => {
      let exceptionThrown = false;
      const mockDbMgrConfig = {
        'misconfigured': null
      };
      try {
        await mongoDbMgr.validateConfig(mockDbMgrConfig);
      } catch (e) {
        expect(e).to.exist;
        exceptionThrown = true;
        expect(e.message).to.contain('dbManagerConfig for connection misconfigured is not an object');
      }
      expect(exceptionThrown).to.equal(true);
    });

    it('should throw an error if dbConfig is not a real object', async () => {
      let exceptionThrown = false;
      const mockDbMgrConfig = '';
      try {
        await mongoDbMgr.validateConfig(mockDbMgrConfig);
      } catch (e) {
        expect(e).to.exist;
        exceptionThrown = true;
        expect(e.message).to.contain('dbManagerConfig is not an object');
      }
      expect(exceptionThrown).to.equal(true);
    });
  });
});


