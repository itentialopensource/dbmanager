/* global pronghornProps */
const { ConnectionManager } = require("./connectionManager");

/*
The purpose of this file is to handle all logic regarding the "singleton"
connection manager.

If we do not have a singleton connection manager created, then the user will
have to instantiate their own ConnectionManager instance. While this is not too
big of a deal, it is one more thing to make the user think about when they
probably already are new to all of this in the first place. Also, the user
might instantiate a new connection manager inside each cog method. This is bad
because one of the core principles of this script is to _cache_ the mongo
connections so we're not constantly opening/closing connections. By
re-instantiating that class, they will be doing the thing we're trying to
avoid. If we create a singleton connection manager for them, then it's harder
(or sometimes impossible) for the user to misuse this script in that way.
*/

/*
We want to expose each and every function of the singleton. That's because one
of the driving principles of this script is to be a very minimal wrapper around
the native mongo client. If we only expose _some_ of the functions of the
singleton, then we will inevitably encapsulate too much logic, and the user
will not be able to do something they want. For example, we once thought to
only expose the 'getDbObject' function, but then someone pointed out that mongo
_client_ objects have events that people will want to attach to. In order to
allow the user to do that, we have to expose the 'getClient' function. At
that point, we realized that it's better to expose everything lest there is
something else that we encapsulate too much.
*/

/*
There is one logistical difficulty in using a singleton. Whenever another
javascript file will "require" this script, the "require" process is expected
to be syncronous and to never throw an exception. If the very act of requiring
a file throws an exception, then the error messages are extremely unhelpful,
and there are probably other undefined ways in which things will break.

The problem is that connecting to mongo is both asynchronous and prone to
exceptions being thrown.

In order to deal with this, we want to dynamically create the singleton the
first time someone tries to access it.

This means we cannot expose the singletone directly, because it won't exist at
"require-time".

Ideally, we don't want to copy/paste function definitions here from
connectionManager.js, but rather dynamically iterate the class's functions and
create a wrapper function around them which deals with the singleton. However,
that will end up with functions that are not properally "named", so we
handcraft the wrapper functions by name.
*/

let connectionManagerSingleton;

// Returns a ConnectionManager object.
function getConnectionManagerSingleton() {
    if (connectionManagerSingleton === undefined) {
        // With 2019.2 or later, service_config is a global that exists and is what is officially supported.
        // Any previous release has to use pronghornProps
        const globalDbManagerConfig = global.service_config && global.service_config.properties && global.service_config.properties.dbManagerConfig || global.pronghornProps && global.pronghornProps.dbManagerConfig;
        // Note: I don't know if it's a better idea to read IAP's global vars here, or while being requir'ed
        connectionManagerSingleton = new ConnectionManager(globalDbManagerConfig);
    }
    return connectionManagerSingleton;
}

// This is used for test purposes
function deleteSingleton() {
  connectionManagerSingleton = undefined;
}

// This is used for test purposes
function getSingleton() {
  return connectionManagerSingleton;
}


/**
 * validateConfig takes config as input so that the API user can take the
 * class and validate config any way they might want to without requiring
 * to create a class instance. Perhaps we should only return a boolean
 * here, and then throw an exception elsewhere, but I want to have good
 * error messages, and a single boolean primative doesn't get us that as
 * easily.
 * @param {object} connectionConfig - the config you want for
 * creating a new connection. From within "dbManagerConfig" object in
 * configuration (see README.md), this function expects only the config
 * for one connection. In the examples from README, you would take the
 * "default" object and pass it in here. The connectionConfig should have
 * a properties called connectionString and connectionOptions.
 * @throws {Exception} - if anything is invalid, an exception is thrown.
 * @return {boolean} - returns true if valid
 */
async function validateConfig(...args) {
    return await getConnectionManagerSingleton().validateConfig(...args);
}

/**
 *  This will retrieve the configuration (connectionString and
 *  connectionOptions) for a connection. Note: all configuration must be
 *  already decrypted by Itential Automation Platform (IAP) for it to be used.
 *  Therefore, the output of this function will have the decrypted values
 *  of any config you encrypted using IAP's encrypt.js script.
 *  @param {string} connectionName - the configured name for the connection
 *  whose config you are wanting.
 *  @return {object} connectionConfig - same output which is valid as input for validateConfig
 */
async function getConnectionConfig(...args) {
    return await getConnectionManagerSingleton().getConnectionConfig(...args);
}

/**
 * Returns a mongo 'client' object. Checks first if the connection
 * already exists. If it does, it returns it. If it does not, it attempts to
 * create one and cache it for reuse.
 * @param {string} connectionName - The configured name for connection you want to use.
 * @returns {object} - The mongo client object: http://mongodb.github.io/node-mongodb-native/2.0/api/MongoClient.html
 */
async function getClient(...args) {
    return await getConnectionManagerSingleton().getClient(...args);
}

/**
 * Returns a Mongo 'db' object. Checks first if the connection
 * already exists. If it does, it uses that same connection. If it does not, it attempts to
 * create a connection (caching it for reuse), and then retrieves and returns the db
 * object for the database you specified.
 * @param {string} dbName - The name of the mongo database you want to use
 * @param {object} [{}] options
 * @param {object} ['default'] options.connectionName - The configured name for connection you want to use.
 *     Normally you don't specify this. Normally you will just use the 'default' connection information which will be
 *     the same connection information used by IAP core (aka: using the same installation of mongo that "mongoProps"
 *     is configured to use).
 * @returns {object} - The mongo db object: http://mongodb.github.io/node-mongodb-native/2.0/api/Db.html
 */
async function getDbObj(...args) {
    return await getConnectionManagerSingleton().getDbObj(...args);
}

/**
 * Attempts to close the specified database connections. If the connections
 * already exists, and remove the connections from the local cache.
 * @returns {boolean} - true if it either successfully closed or wasn't already created
 */
async function closeConnection(...args) {
    return await getConnectionManagerSingleton().closeConnection(...args);
}

/**
 * Attempts to close ALL database connections. If the connections
 * already exists, and remove the connections from the local cache.
 * @returns {boolean} - false if at least 1 failed to close. Otherwise true.
 */
async function closeAllConnections(...args) {
    return await getConnectionManagerSingleton().closeAllConnections(...args);
}

module.exports = {
  // We join all the singleton functions into an object here, so that the
  // 'jsdoc' tool works. TODO: fix jsdoc creation process to remove more
  // boilerplate
  singletonFunctions: {
    validateConfig,
    getConnectionConfig,
    getClient,
    getDbObj,
    closeConnection,
    closeAllConnections,
  },
  // We expose this because our integration tests need to do "whitebox" testing.
  // These will not be publicly exposed by the module.
  getSingleton,
  deleteSingleton,
};
